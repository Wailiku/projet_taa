package entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ville")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ville.findAll", query = "SELECT a FROM Ville a"),
    @NamedQuery(name = "Ville.findByNom", query = "SELECT a FROM Ville a WHERE a.nom = :nom"),
    @NamedQuery(name = "Ville.findById", query = "SELECT a FROM Ville a WHERE a.id = :id")})
public class Ville extends Lieu {
	
	@ManyToOne
	private Departement departement;

	public Ville(String nom, Departement departement) {
		super(nom);
		this.departement = departement;
	}

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
}
