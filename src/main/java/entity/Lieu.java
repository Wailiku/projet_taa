package entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import java.util.List;

import javax.persistence.Column;

@Entity
public abstract class Lieu {
	
	@Id 
	@GeneratedValue
	protected Long id;
	
	@Column(length=80,nullable=false)
	protected String nom;
	
	@ManyToMany(mappedBy="lieux")
	protected List<Personne> personnes;
	
	private int temperature;
	private int humidite;
	private int vent;
	private String temps;
	
	//Le constructeur prend en parametre le nom du lieu
	public Lieu(String nom) {
		this.nom = nom;
	}
	
	public Long getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getTemperature() {
		return temperature;
	}
	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}
	public int getHumidite() {
		return humidite;
	}
	public void setHumidite(int humidite) {
		this.humidite = humidite;
	}
	public int getVent() {
		return vent;
	}
	public void setVent(int vent) {
		this.vent = vent;
	}
	public String getTemps() {
		return temps;
	}
	public void setTemps(String temps) {
		this.temps = temps;
	}

	public List<Personne> getPersonnes() {
		return personnes;
	}

	public void setPersonnes(List<Personne> personnes) {
		this.personnes = personnes;
	}
	
	
	
	
}
