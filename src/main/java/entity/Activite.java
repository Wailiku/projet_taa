package entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "activite")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Activite.findAll", query = "SELECT a FROM Activite a"),
    @NamedQuery(name = "Activite.findByNom", query = "SELECT a FROM Activite a WHERE a.nom = :nom"),
    @NamedQuery(name = "Activite.findById", query = "SELECT a FROM Activite a WHERE a.id = :id")})
public class Activite {

    private Long id;
    private String nom;
	
    private int tempMax;
    private int tempMin;
	
    private List<String> temps;
    private List<Personne> personnes;
    
    private int ventMax;
    private int ventMin;
    private int humiditeMax;
    private int humiditeMin;

    public Activite() {
        super();
    }

    public Activite(String name) {
    	
        this.nom = name;
    }
    
    public Activite(String nom, int tempMax, int tempMin, int ventMax, int ventMin,
    		int humiditeMax, int humiditeMin, List<String> temps) {
        this.nom = nom;
        this.tempMax = tempMax;
        this.tempMin = tempMin;
        this.ventMax = ventMax;
        this.ventMin = ventMin;
        this.humiditeMax = humiditeMax;
        this.humiditeMin = humiditeMin;
        this.temps = temps;
    }

    @Id 
    @GeneratedValue
	protected Long getId() {
		return id;
	}
	protected void setId(Long id) {
		this.id = id;
	}

	@Column(length=80,nullable=false)
	public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

	public int getTempMax() {
		return tempMax;
	}
	public void setTempMax(int tempMax) {
		this.tempMax = tempMax;
	}
	public int getTempMin() {
		return tempMin;
	}
	public void setTempMin(int tempMin) {
		this.tempMin = tempMin;
	}
	
	@Transient
	public List<String> getTemps() {
		return temps;
	}
	public void setTemps(List<String> temps) {
		this.temps = temps;
	}

	@ManyToMany(mappedBy="activites")
	public List<Personne> getPersonnes() {
		return personnes;
	}

	public void setPersonnes(List<Personne> personnes) {
		this.personnes = personnes;
	}

	protected String getTempsBD() {
		 StringJoiner sj = new StringJoiner(" , ");
		 for (String s : getTemps())
			 sj.add(s);
		 return  sj.toString();
	}
	
	protected void setTempsBD(String temps) {
		this.temps =  new ArrayList<String>(Arrays.asList(temps.split(" , ")));
	}

	public int getVentMax() {
		return ventMax;
	}
	public void setVentMax(int ventMax) {
		this.ventMax = ventMax;
	}
	public int getVentMin() {
		return ventMin;
	}
	public void setVentMin(int ventMin) {
		this.ventMin = ventMin;
	}
	public int getHumiditeMax() {
		return humiditeMax;
	}
	public void setHumiditeMax(int humiditeMax) {
		this.humiditeMax = humiditeMax;
	}
	public int getHumiditeMin() {
		return humiditeMin;
	}
	public void setHumiditeMin(int humidite_min) {
		this.humiditeMin = humidite_min;
	}
}
