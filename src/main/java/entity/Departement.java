package entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "departement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Departement.findAll", query = "SELECT a FROM Departement a"),
    @NamedQuery(name = "Departement.findByNom", query = "SELECT a FROM Departement a WHERE a.nom = :nom"),
    @NamedQuery(name = "Departement.findById", query = "SELECT a FROM Departement a WHERE a.id = :id")})
public class Departement extends Lieu {
	
	@OneToMany(mappedBy="departement")
	private List<Ville> villes;
	@ManyToOne
	private Region region;
	
	public Departement(String nom, Region region) {
		super(nom);
		this.region = region;
	}

	public List<Ville> getVilles() {
		return villes;
	}

	public void setVilles(List<Ville> villes) {
		this.villes = villes;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
		
	}
}
