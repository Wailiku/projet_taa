package entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "region")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Region.findAll", query = "SELECT a FROM Region a"),
    @NamedQuery(name = "Region.findByNom", query = "SELECT a FROM Region a WHERE a.nom = :nom"),
    @NamedQuery(name = "Region.findById", query = "SELECT a FROM Region a WHERE a.id = :id")})

public class Region extends Lieu {
	
	@OneToMany(mappedBy="region")
	private List<Departement> departements;

	public Region(String nom) {
		super(nom);
	}
	public Region(String nom, List<Departement> departements) {
		super(nom);
		this.departements = departements;
	}

	public List<Departement> getDepartements() {
		return departements;
	}

	public void setDepartements(List<Departement> departements) {
		this.departements = departements;
	}
}