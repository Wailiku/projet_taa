package dao;

import java.util.List;

import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import entity.Activite;
import entity.Departement;
import entity.Personne;
import entity.Region;

public class DaoActivite extends DaoImplGeneral<Activite, Long> {

	public DaoActivite(Class<Activite> type) {
		super(type);
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public void delete(Activite activite) {
		
		EntityTransaction et = em.getTransaction();
		
		et.begin();
		if(activite.getPersonnes() != null) {
			for(Personne personne : activite.getPersonnes()) {
				personne.getLieux().remove(activite);
			}
		}
		
		em.remove(activite);
		et.commit(); 
	}
}
