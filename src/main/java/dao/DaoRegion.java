package dao;

import javax.persistence.EntityTransaction;

import entity.Departement;
import entity.Personne;
import entity.Region;

public class DaoRegion extends DaoImplGeneral<Region, Long>{

	public DaoRegion(Class<Region> type) {
		super(type);
	}
	
	public void delete(Region region) {
		
		EntityTransaction et = em.getTransaction();
		
		et.begin();
		if(region.getDepartements() != null) {
			for(Departement d : region.getDepartements()) {
				d.setRegion(null);
			}
		}
		
		if(region.getPersonnes() != null) {
			for(Personne personne : region.getPersonnes()) {
				personne.getLieux().remove(region);
			}
		}
		
		
		em.remove(region);
		
		et.commit(); 
	}
}
