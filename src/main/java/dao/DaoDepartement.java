package dao;

import javax.persistence.EntityTransaction;

import entity.Departement;
import entity.Lieu;
import entity.Personne;
import entity.Region;
import entity.Ville;

public class DaoDepartement extends DaoImplGeneral<Departement, Long>{

	public DaoDepartement(Class<Departement> type) {
		super(type);
	}

	public void delete(Departement departement) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		Region r = departement.getRegion();
		if(r != null)
			r.getDepartements().remove(departement);

		for(Ville v : departement.getVilles()) {
			v.setDepartement(null);
		}
		for(Personne p : departement.getPersonnes()) {
			p.getLieux().remove(departement);
		}
		em.remove(departement);
		et.commit();
	}
}
