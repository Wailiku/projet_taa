package dao;

import javax.persistence.EntityTransaction;

import entity.Departement;
import entity.Lieu;
import entity.Personne;
import entity.Ville;

public class DaoVille extends DaoImplGeneral<Ville, Long>{

	public DaoVille(Class<Ville> type) {
		super(type);
	}

	public void delete(Ville ville) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		Departement d = ville.getDepartement();
		if(d != null)
			d.getVilles().remove(ville);
		for(Personne p : ville.getPersonnes()) {
			p.getLieux().remove(ville);
		}
		em.remove(ville);
		et.commit();
	}
}
