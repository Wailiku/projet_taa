package dao;

import javax.persistence.EntityTransaction;

import entity.Personne;

public class DaoPersonne extends DaoImplGeneral<Personne, Long>{

	public DaoPersonne(Class<Personne> type) {
		super(type);
	}
	
	@Override
	public void delete(Personne personne) {
		EntityTransaction et = em.getTransaction();
		
		et.begin();
		personne.getLieux().clear();
		personne.getActivites().clear();
		em.remove(personne);
		et.commit();		
	}
}
