package dao;

import java.io.Serializable;
import java.util.List;

public interface DaoInterface<T, PK extends Serializable>{
	
	void create(T newInstance);
	
	T findOneById(PK id);
	
	T findOneByName(String nom);
	
	List<T> findAll();
	
	T update(T transientObject);
	
	void delete(T persistentObject);
	
	void deleteById(PK id);

}