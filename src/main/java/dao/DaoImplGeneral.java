package dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import jpa.EntityManagerHelper;

public class DaoImplGeneral<T,PK extends Serializable> implements DaoInterface<T, PK>{

	protected Class<T> type;
	
	@PersistenceContext
	EntityManager em;
	
	public DaoImplGeneral(Class<T> type) {
		this.type = type;
		this.em = EntityManagerHelper.getEntityManager();
	}
	
	
	@Override
	public void create(T newInstance) {
		em.persist(newInstance);
		System.out.println("Bonjour");
	}

	@Override
	public T findOneById(PK id) {
		return em.find(type, id);
	}
	
	@Override
	public T findOneByName(String nom) {
		return em.find(type, nom);
	}

	@Override
	public List<T> findAll() {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(type);
		Root<T> from = query.from(type);
		query.select(from);
		return em.createQuery(query).getResultList();
	}

	@Override
	public  T update(T transientObject) {
		return em.merge(transientObject);
	}

	@Override
	public void delete(T persistentObject) {
		EntityManagerHelper.beginTransaction();
		em.remove(persistentObject);
		EntityManagerHelper.commit();
	}


	@Override
	public void deleteById(PK id) {
		T entity = findOneById(id);
		delete(entity);
	}
	

}