package jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import dao.DaoActivite;
import entity.*;

public class JpaTest {

    private EntityManager manager;

    public JpaTest(EntityManager manager) {
        this.manager = manager;
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("mysql");
        EntityManager manager = factory.createEntityManager();
        JpaTest test = new JpaTest(manager);

        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        try {
            test.createPersonnes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();

        
        //test.listEmployees();
            
        manager.close();
        factory.close();
        System.out.println(".. done");
    }
    
    private void createPersonnes() {
        int numOfPersonnes = 0;//manager.createQuery("Select a From Personne a", Personne.class).getResultList().size();
        if (numOfPersonnes == 0) {
        	Region region = new Region ("Bretagne");
        	manager.persist(region);
        	Departement departement = new Departement("Ille et Vilaine", region);
        	manager.persist(departement);
            Ville ville = new Ville("Rennes",departement);
            manager.persist(ville);
            Activite badminton = new Activite("Badminton", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(badminton);
            Activite soccer =new Activite("Soccer", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(soccer);
            List<Lieu> lieux = new ArrayList<Lieu>();
            lieux.add(ville);
            List<Activite> activites1 = new ArrayList<Activite>();
            activites1.add(badminton);
            List<Activite> activites2 = new ArrayList<Activite>();
            activites1.add(soccer);
            
            manager.merge(new Personne("Jakab Gipsz", "adresse 1", "1234", lieux, activites1));
            manager.merge(new Personne("Captain Nemo", "adresse 2", "5678", lieux, activites2));
            
            
        }
    }
}
