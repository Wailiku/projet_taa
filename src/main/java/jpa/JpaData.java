package jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import dao.DaoActivite;
import entity.*;

public class JpaData {

    private EntityManager manager;

    public JpaData(EntityManager manager) {
        this.manager = manager;
    }
    /**
     * @param args
     */
    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("mysql");
        EntityManager manager = factory.createEntityManager();
        JpaData dataFiller = new JpaData(manager);

        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Activite aviron = new Activite("Aviron", 40, 5, 50, 0, 80, 0, new ArrayList<String>());
        manager.persist(aviron);
        try {
        	int numOfLieux = 0; //manager.createQuery("Select a From Lieu a", Lieu.class).getResultList().size();
            if (numOfLieux == 0) {
            	/*Region ARA = new Region ("Auvergne-Rhône-Alpes");
            	manager.persist(ARA);
            	Departement Ain = new Departement("Ain",ARA);
            	manager.persist(Ain);
            	Ville BourgEnBresse = new Ville("Bourg-en-Bresse",Ain);
            	manager.persist(BourgEnBresse);
            	Ville Oyonnax = new Ville("Oyonnax",Ain);
            	manager.persist(Oyonnax);
            	Departement Allier = new Departement("Allier",ARA);
            	manager.persist(Allier);
            	Ville Montlucon = new Ville("Montluçon",Allier);
            	manager.persist(Montlucon);
            	Ville Vichy = new Ville("Vichy",Allier);
            	manager.persist(Vichy);
            	Departement Ardeche = new Departement("Ardèche",ARA);
            	manager.persist(Ardeche);
            	Ville Annonay = new Ville("Annonay",Ardeche);
            	manager.persist(Annonay);
            	Ville Aubenas = new Ville("Aubenas",Ardeche);
            	manager.persist(Aubenas);
            	Departement Cantal = new Departement("Cantal",ARA);
            	manager.persist(Cantal);
            	Ville Aurillac = new Ville("Aurillac",Cantal);
            	manager.persist(Aurillac);
            	Departement Drome = new Departement("Drôme",ARA);
            	manager.persist(Drome);
            	Ville Valence = new Ville("Valence",Drome);
            	manager.persist(Valence);
            	Ville Montelimar = new Ville("Montélimar",Drome);
            	manager.persist(Montelimar);
            	Departement Isere = new Departement("Isère",ARA);
            	manager.persist(Isere);
            	Ville Grenoble = new Ville("Grenoble",Isere);
            	manager.persist(Grenoble);
            	Ville Vienne = new Ville("Vienne",Isere);
            	manager.persist(Vienne);
            	Departement Loire = new Departement("Loire",ARA);
            	manager.persist(Loire);
            	Ville StEtienne = new Ville("Saint-Étienne",Loire);
            	manager.persist(StEtienne);
            	Departement HteLoire = new Departement("Haute-Loire",ARA);
            	manager.persist(HteLoire);
            	Ville LePuyVelay = new Ville("Le Puy-en-Velay",HteLoire);
            	manager.persist(LePuyVelay);
            	Departement PuyDeDome = new Departement("Puy-de-Dôme",ARA);
            	manager.persist(PuyDeDome);
            	Ville Clermont = new Ville("Clermont-Ferrand",PuyDeDome);
            	manager.persist(Clermont);
            	Departement Rhone = new Departement("Rhône",ARA);
            	manager.persist(Rhone);
            	Ville Villefranche = new Ville("Villefranche-sur-Saône",Rhone);
            	manager.persist(Villefranche);
            	Departement MetropoleLyon = new Departement("Métropole de Lyon",ARA);
            	manager.persist(MetropoleLyon);
            	Ville Lyon = new Ville("Lyon",MetropoleLyon);
            	manager.persist(Lyon);
            	Departement Savoie = new Departement("Savoie",ARA);
            	manager.persist(Savoie);
            	Ville Chambery = new Ville("Chambéry",Savoie);
            	manager.persist(Chambery);
            	Departement HteSavoie = new Departement("Haute-Savoie",ARA);
            	manager.persist(HteSavoie);
            	Ville Annecy = new Ville("Annecy",HteSavoie);
            	manager.persist(Annecy);
            	*/
            	
            	/*
            	Region BFC = new Region ("Bourgogne-Franche-Comté");
            	manager.persist(BFC);
            	*/
            	
            	Region BRE = new Region ("Bretagne");
            	manager.persist(BRE);
            	Departement CoteArmor = new Departement("Côtes-d'Armor",BRE);
            	manager.persist(CoteArmor);
            	Ville SaintBrieuc = new Ville("Saint-Brieuc",CoteArmor);
            	manager.persist(SaintBrieuc);
            	Ville Lannion = new Ville("Lannion",CoteArmor);
            	manager.persist(Lannion);
            	//Ville Dinan = new Ville("Dinan",CoteArmor);
            	//manager.persist(Dinan);
            	Departement Finistere = new Departement("Finistère",BRE);
            	manager.persist(Finistere);
            	Ville Brest = new Ville("Brest",Finistere);
            	manager.persist(Brest);
            	Ville Quimper = new Ville("Quimper",Finistere);
            	manager.persist(Quimper);
            	Ville Morlaix = new Ville("Morlaix",Finistere);
            	manager.persist(Morlaix);
            	Departement IlleVilaine = new Departement("Ille-et-Vilaine",BRE);
            	manager.persist(IlleVilaine);
            	//Ville Rennes = new Ville("Rennes",IlleVilaine);
            	//manager.persist(Rennes);
            	Ville StMalo = new Ville("StMalo",IlleVilaine);
            	manager.persist(StMalo);
            	//Ville Bruz = new Ville("Bruz",IlleVilaine);
            	//manager.persist(Bruz);
            	//Ville Fougeres = new Ville("Fougères",IlleVilaine);
            	//manager.persist(Fougeres);
            	//Ville Redon = new Ville("Redon",IlleVilaine);
            	//manager.persist(Redon);
            	Departement Morbihan = new Departement("Morbihan",BRE);
            	manager.persist(Morbihan);
            	Ville Vannes = new Ville("Vannes",Morbihan);
            	manager.persist(Vannes);
            	Ville Lorient = new Ville("Lorient",Morbihan);
            	manager.persist(Lorient);
            	//Ville Pontivy = new Ville("Pontivy",Morbihan);
            	//manager.persist(Pontivy);
            	
            	/*
            	Region CVL = new Region ("Centre-Val de Loire");
            	manager.persist(CVL);
            	*/
            	
            	Region COR = new Region ("Corse");
            	manager.persist(COR);
            	Departement CorseDuSud = new Departement("Corse-du-Sud",COR);
            	manager.persist(CorseDuSud);
            	Ville Ajaccio = new Ville("Ajaccio",CorseDuSud);
            	manager.persist(Ajaccio);
            	Departement HteCorse = new Departement("Haute-Corse",COR);
            	manager.persist(HteCorse);
            	Ville Bastia = new Ville("Bastia",HteCorse);
            	manager.persist(Bastia);
            }
            int numOfActivites = 0;//manager.createQuery("Select a From Activite a", Activite.class).getResultList().size();
            if (numOfActivites == 0) {
            	//Individuels
                //Activite accrobranche = new Activite("Accrobranche", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(accrobranche);
                //Activite alpinisme = new Activite("Alpinisme", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(alpinisme);
                //Activite athletisme = new Activite("Athlétisme", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(athletisme);
                
                //Activite ballTrap = new Activite("Ball Trap", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(ballTrap);
                //Activite biathlon = new Activite("Biathlon", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(biathlon);
                Activite canoeKayak = new Activite("Canöe-Kayak", 40, 5, 50, 0, 80, 0, new ArrayList<String>());
                manager.persist(canoeKayak);
                //Activite canyonisme = new Activite("Canyonisme", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(canyonisme);
                Activite charAVoile = new Activite("Char à Voile", 40, 5, 80, 25, 80, 0, new ArrayList<String>());
                manager.persist(charAVoile);
                //Activite CourseAPied = new Activite("Course à Pied", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(CourseAPied);
                //Activite cycloCross = new Activite("Cyclo-Cross", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(cycloCross);
                //Activite cyclotourisme = new Activite("Cyclotourisme", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(cyclotourisme);
                //Activite equitation = new Activite("Équitation", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(equitation);
                //Activite escalade = new Activite("Escalade", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(escalade);
                //Activite golf = new Activite("Golf", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(golf);
                Activite kitesurf = new Activite("Kitesurf", 40, 5, 80, 25, 80, 0, new ArrayList<String>());
                manager.persist(kitesurf);
                //Activite luge = new Activite("Luge", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(luge);
                //Activite marcheAPied = new Activite("Marche à Pied", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(marcheAPied);
                //Activite penthatlon = new Activite("Penthatlon", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(penthatlon);
                Activite plancheAVoile = new Activite("Planche à Voile", 40, 5, 80, 25, 80, 0, new ArrayList<String>());
                manager.persist(plancheAVoile);
                //Activite rafting = new Activite("Rafting", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(rafting);
                //Activite randonnee = new Activite("Randonnée Pédestre", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(randonnee);
                //Activite skiAlpin = new Activite("Ski Alpin", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(skiAlpin);
                Activite skiNautique = new Activite("Ski Nautique", 40, 5, 50, 0, 80, 0, new ArrayList<String>());
                manager.persist(skiNautique);
                //Activite skiDeFond = new Activite("Ski de Fond", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(skiDeFond);
                //Activite snowboard = new Activite("Snowboard", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(snowboard);
                //Activite speleologie = new Activite("Spéléologie", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(speleologie);
                Activite surf =new Activite("Surf", 40, 5, 50, 5, 80, 0, new ArrayList<String>());
                manager.persist(surf);
                //Activite triathlon = new Activite("Triathlon", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(triathlon);
                Activite voile = new Activite("Voile", 40, 5, 80, 25, 80, 0, new ArrayList<String>());
                manager.persist(voile);
                //Activite vtt = new Activite("VTT", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
                //manager.persist(vtt);
                Activite wakeboard = new Activite("Wakeboard", 40, 5, 50, 0, 80, 0, new ArrayList<String>());
                manager.persist(wakeboard);
            }
            //dataFiller.createLieux();
            //dataFiller.createActivites();
            //dataFiller.createPersonnes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        
        DaoActivite daoactivite = new DaoActivite(Activite.class);
        aviron.setTempMax(200);
        tx.begin();
        daoactivite.update(aviron);
        tx.commit();
		for(Activite a : daoactivite.findAll()) {
			System.out.println("Activite : \n"+a.getTempMax());
		}
            
        manager.close();
        factory.close();
        System.out.println(".. done");
    }
    
    private void createLieux() {
        int numOfLieux = 0; //manager.createQuery("Select a From Lieu a", Lieu.class).getResultList().size();
        if (numOfLieux == 0) {
        	/*Region ARA = new Region ("Auvergne-Rhône-Alpes");
        	manager.persist(ARA);
        	Departement Ain = new Departement("Ain",ARA);
        	manager.persist(Ain);
        	Ville BourgEnBresse = new Ville("Bourg-en-Bresse",Ain);
        	manager.persist(BourgEnBresse);
        	Ville Oyonnax = new Ville("Oyonnax",Ain);
        	manager.persist(Oyonnax);
        	Departement Allier = new Departement("Allier",ARA);
        	manager.persist(Allier);
        	Ville Montlucon = new Ville("Montluçon",Allier);
        	manager.persist(Montlucon);
        	Ville Vichy = new Ville("Vichy",Allier);
        	manager.persist(Vichy);
        	Departement Ardeche = new Departement("Ardèche",ARA);
        	manager.persist(Ardeche);
        	Ville Annonay = new Ville("Annonay",Ardeche);
        	manager.persist(Annonay);
        	Ville Aubenas = new Ville("Aubenas",Ardeche);
        	manager.persist(Aubenas);
        	Departement Cantal = new Departement("Cantal",ARA);
        	manager.persist(Cantal);
        	Ville Aurillac = new Ville("Aurillac",Cantal);
        	manager.persist(Aurillac);
        	Departement Drome = new Departement("Drôme",ARA);
        	manager.persist(Drome);
        	Ville Valence = new Ville("Valence",Drome);
        	manager.persist(Valence);
        	Ville Montelimar = new Ville("Montélimar",Drome);
        	manager.persist(Montelimar);
        	Departement Isere = new Departement("Isère",ARA);
        	manager.persist(Isere);
        	Ville Grenoble = new Ville("Grenoble",Isere);
        	manager.persist(Grenoble);
        	Ville Vienne = new Ville("Vienne",Isere);
        	manager.persist(Vienne);
        	Departement Loire = new Departement("Loire",ARA);
        	manager.persist(Loire);
        	Ville StEtienne = new Ville("Saint-Étienne",Loire);
        	manager.persist(StEtienne);
        	Departement HteLoire = new Departement("Haute-Loire",ARA);
        	manager.persist(HteLoire);
        	Ville LePuyVelay = new Ville("Le Puy-en-Velay",HteLoire);
        	manager.persist(LePuyVelay);
        	Departement PuyDeDome = new Departement("Puy-de-Dôme",ARA);
        	manager.persist(PuyDeDome);
        	Ville Clermont = new Ville("Clermont-Ferrand",PuyDeDome);
        	manager.persist(Clermont);
        	Departement Rhone = new Departement("Rhône",ARA);
        	manager.persist(Rhone);
        	Ville Villefranche = new Ville("Villefranche-sur-Saône",Rhone);
        	manager.persist(Villefranche);
        	Departement MetropoleLyon = new Departement("Métropole de Lyon",ARA);
        	manager.persist(MetropoleLyon);
        	Ville Lyon = new Ville("Lyon",MetropoleLyon);
        	manager.persist(Lyon);
        	Departement Savoie = new Departement("Savoie",ARA);
        	manager.persist(Savoie);
        	Ville Chambery = new Ville("Chambéry",Savoie);
        	manager.persist(Chambery);
        	Departement HteSavoie = new Departement("Haute-Savoie",ARA);
        	manager.persist(HteSavoie);
        	Ville Annecy = new Ville("Annecy",HteSavoie);
        	manager.persist(Annecy);
        	*/
        	
        	/*
        	Region BFC = new Region ("Bourgogne-Franche-Comté");
        	manager.persist(BFC);
        	*/
        	
        	Region BRE = new Region ("Bretagne");
        	manager.persist(BRE);
        	Departement CoteArmor = new Departement("Côtes-d'Armor",BRE);
        	manager.persist(CoteArmor);
        	Ville SaintBrieuc = new Ville("Saint-Brieuc",CoteArmor);
        	manager.persist(SaintBrieuc);
        	Ville Lannion = new Ville("Lannion",CoteArmor);
        	manager.persist(Lannion);
        	//Ville Dinan = new Ville("Dinan",CoteArmor);
        	//manager.persist(Dinan);
        	Departement Finistere = new Departement("Finistère",BRE);
        	manager.persist(Finistere);
        	Ville Brest = new Ville("Brest",Finistere);
        	manager.persist(Brest);
        	Ville Quimper = new Ville("Quimper",Finistere);
        	manager.persist(Quimper);
        	Ville Morlaix = new Ville("Morlaix",Finistere);
        	manager.persist(Morlaix);
        	Departement IlleVilaine = new Departement("Ille-et-Vilaine",BRE);
        	manager.persist(IlleVilaine);
        	//Ville Rennes = new Ville("Rennes",IlleVilaine);
        	//manager.persist(Rennes);
        	Ville StMalo = new Ville("StMalo",IlleVilaine);
        	manager.persist(StMalo);
        	//Ville Bruz = new Ville("Bruz",IlleVilaine);
        	//manager.persist(Bruz);
        	//Ville Fougeres = new Ville("Fougères",IlleVilaine);
        	//manager.persist(Fougeres);
        	//Ville Redon = new Ville("Redon",IlleVilaine);
        	//manager.persist(Redon);
        	Departement Morbihan = new Departement("Morbihan",BRE);
        	manager.persist(Morbihan);
        	Ville Vannes = new Ville("Vannes",Morbihan);
        	manager.persist(Vannes);
        	Ville Lorient = new Ville("Lorient",Morbihan);
        	manager.persist(Lorient);
        	//Ville Pontivy = new Ville("Pontivy",Morbihan);
        	//manager.persist(Pontivy);
        	
        	/*
        	Region CVL = new Region ("Centre-Val de Loire");
        	manager.persist(CVL);
        	*/
        	
        	Region COR = new Region ("Corse");
        	manager.persist(COR);
        	Departement CorseDuSud = new Departement("Corse-du-Sud",COR);
        	manager.persist(CorseDuSud);
        	Ville Ajaccio = new Ville("Ajaccio",CorseDuSud);
        	manager.persist(Ajaccio);
        	Departement HteCorse = new Departement("Haute-Corse",COR);
        	manager.persist(HteCorse);
        	Ville Bastia = new Ville("Bastia",HteCorse);
        	manager.persist(Bastia);
        	
        	/*
        	Region GES = new Region ("Grand Est");
        	manager.persist(GES);
        	
        	Region HDF = new Region ("Hauts-de-France");
        	manager.persist(HDF);
        	
        	Region IDF = new Region ("Île-de-France");
        	manager.persist(IDF);
        	
        	Region NOR = new Region ("Normandie");
        	manager.persist(NOR);
        	
        	Region NAQ = new Region ("Nouvelle-Aquitaine");
        	manager.persist(NAQ);
        	
        	Region OCC = new Region ("Occitanie");
        	manager.persist(OCC);
        	
        	Region PDL = new Region ("Pays de la Loire");
        	manager.persist(PDL);
        	
        	Region PAC = new Region ("Provence-Alpes-Côte d'Azur");
        	manager.persist(PAC);
        	
        	Region GP = new Region ("Guadeloupe");
        	manager.persist(GP);
        	
        	Region GF = new Region ("Guyane");
        	manager.persist(GF);
        	
        	Region MQ = new Region ("Martinique");
        	manager.persist(MQ);
        	
        	Region RE = new Region ("La Réunion");
        	manager.persist(RE);
        	
        	Region YT = new Region ("Mayotte");
        	manager.persist(YT);
        	*/
        }
    }
    
    private void createActivites() {
        int numOfActivites = 0;//manager.createQuery("Select a From Activite a", Activite.class).getResultList().size();
        if (numOfActivites == 0) {
        	//Individuels
            //Activite accrobranche = new Activite("Accrobranche", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(accrobranche);
            //Activite alpinisme = new Activite("Alpinisme", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(alpinisme);
            //Activite athletisme = new Activite("Athlétisme", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(athletisme);
            Activite aviron = new Activite("Aviron", 40, 5, 50, 0, 80, 0, new ArrayList<String>());
            manager.persist(aviron);
            //Activite ballTrap = new Activite("Ball Trap", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(ballTrap);
            //Activite biathlon = new Activite("Biathlon", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(biathlon);
            Activite canoeKayak = new Activite("Canöe-Kayak", 40, 5, 50, 0, 80, 0, new ArrayList<String>());
            manager.persist(canoeKayak);
            //Activite canyonisme = new Activite("Canyonisme", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(canyonisme);
            Activite charAVoile = new Activite("Char à Voile", 40, 5, 80, 25, 80, 0, new ArrayList<String>());
            manager.persist(charAVoile);
            //Activite CourseAPied = new Activite("Course à Pied", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(CourseAPied);
            //Activite cycloCross = new Activite("Cyclo-Cross", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(cycloCross);
            //Activite cyclotourisme = new Activite("Cyclotourisme", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(cyclotourisme);
            //Activite equitation = new Activite("Équitation", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(equitation);
            //Activite escalade = new Activite("Escalade", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(escalade);
            //Activite golf = new Activite("Golf", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(golf);
            Activite kitesurf = new Activite("Kitesurf", 40, 5, 80, 25, 80, 0, new ArrayList<String>());
            manager.persist(kitesurf);
            //Activite luge = new Activite("Luge", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(luge);
            //Activite marcheAPied = new Activite("Marche à Pied", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(marcheAPied);
            //Activite penthatlon = new Activite("Penthatlon", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(penthatlon);
            Activite plancheAVoile = new Activite("Planche à Voile", 40, 5, 80, 25, 80, 0, new ArrayList<String>());
            manager.persist(plancheAVoile);
            //Activite rafting = new Activite("Rafting", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(rafting);
            //Activite randonnee = new Activite("Randonnée Pédestre", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(randonnee);
            //Activite skiAlpin = new Activite("Ski Alpin", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(skiAlpin);
            Activite skiNautique = new Activite("Ski Nautique", 40, 5, 50, 0, 80, 0, new ArrayList<String>());
            manager.persist(skiNautique);
            //Activite skiDeFond = new Activite("Ski de Fond", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(skiDeFond);
            //Activite snowboard = new Activite("Snowboard", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(snowboard);
            //Activite speleologie = new Activite("Spéléologie", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(speleologie);
            Activite surf =new Activite("Surf", 40, 5, 50, 5, 80, 0, new ArrayList<String>());
            manager.persist(surf);
            //Activite triathlon = new Activite("Triathlon", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(triathlon);
            Activite voile = new Activite("Voile", 40, 5, 80, 25, 80, 0, new ArrayList<String>());
            manager.persist(voile);
            //Activite vtt = new Activite("VTT", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            //manager.persist(vtt);
            Activite wakeboard = new Activite("Wakeboard", 40, 5, 50, 0, 80, 0, new ArrayList<String>());
            manager.persist(wakeboard);
            
            //Collectifs
            /*
            Activite baseball = new Activite("Baseball", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(baseball);
            Activite beachHandball = new Activite("Beach Handball", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(beachHandball);
            Activite beachSoccer = new Activite("Beach Soccer", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(beachSoccer);
            Activite beachVolley = new Activite("Beach Volley", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(beachVolley);
            Activite cricket = new Activite("Cricket", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(cricket);
            Activite crosseAuChamp = new Activite("Crosse au Champ", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(crosseAuChamp);
            Activite football = new Activite("Football", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(football);
            Activite footballAustralien = new Activite("Football Australien", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(footballAustralien);
            Activite footballAmericain = new Activite("Football Americain", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(footballAmericain);
            Activite footballCanadien = new Activite("Football Canadien", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(footballCanadien);
            Activite footballGaelique = new Activite("Football Gaëlique", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(footballGaelique);
            Activite hockeySurGazon = new Activite("Hockey sur Gazon", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(hockeySurGazon);
            Activite rugbyVII = new Activite("Rugby à VII", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(rugbyVII);
            Activite rugbyXIII = new Activite("Rugby à XIII", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(rugbyXIII);
            Activite rugbyXV = new Activite("Rugby à XV", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(rugbyXV);
            Activite softball = new Activite("Softball", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(softball);
            Activite ultimate = new Activite("Ultimate", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(ultimate);
            Activite rollerDerby = new Activite("Roller Derby", 40, 5, 50, -50, 50, 0, new ArrayList<String>());
            manager.persist(rollerDerby);
            */
            
            int numOfPersonnes = 0;//manager.createQuery("Select a From Personne a", Personne.class).getResultList().size();
            if (numOfPersonnes == 0) {
            	Region region = new Region ("Bretagne");
            	manager.persist(region);
            	Departement departement = new Departement("Ille et Vilaine", region);
            	manager.persist(departement);
                Ville ville = new Ville("Rennes",departement);
                manager.persist(ville);
                manager.persist(surf);
                List<Lieu> lieux = new ArrayList<Lieu>();
                lieux.add(ville);
                List<Activite> activites1 = new ArrayList<Activite>();
                activites1.add(plancheAVoile);
                List<Activite> activites2 = new ArrayList<Activite>();
                activites1.add(surf);
                
                manager.merge(new Personne("Jakab Gipsz", "adresse 1", "1234", lieux, activites1));
                manager.merge(new Personne("Captain Nemo", "adresse 2", "5678", lieux, activites2));
            }
        }
    }
    
    private void createPersonnes() {
        int numOfPersonnes = 0;//manager.createQuery("Select a From Personne a", Personne.class).getResultList().size();
        if (numOfPersonnes == 0) {
        	Region region = new Region ("Bretagne");
        	manager.persist(region);
        	Departement departement = new Departement("Ille et Vilaine", region);
        	manager.persist(departement);
            Ville ville = new Ville("Rennes",departement);
            manager.persist(ville);
            Activite plancheAVoile = new Activite("Planche à Voile", 40, 5, 80, 25, 80, 0, new ArrayList<String>());
            manager.persist(plancheAVoile);
            Activite surf =new Activite("Surf", 40, 5, 50, 5, 80, 0, new ArrayList<String>());
            manager.persist(surf);
            List<Lieu> lieux = new ArrayList<Lieu>();
            lieux.add(ville);
            List<Activite> activites1 = new ArrayList<Activite>();
            activites1.add(plancheAVoile);
            List<Activite> activites2 = new ArrayList<Activite>();
            activites1.add(surf);
            
            manager.merge(new Personne("Jakab Gipsz", "adresse 1", "1234", lieux, activites1));
            manager.merge(new Personne("Captain Nemo", "adresse 2", "5678", lieux, activites2));
        }
    }
}
