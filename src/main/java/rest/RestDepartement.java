package rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dao.DaoDepartement;
import entity.Departement;

@Path("/departement")
public class RestDepartement {
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Departement> getDepartements(){
		DaoDepartement daodepartement = new DaoDepartement(Departement.class);
		return daodepartement.findAll();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Departement getDepartementById(@PathParam("id") long id) {
		DaoDepartement daodepartement = new DaoDepartement(Departement.class);
		return daodepartement.findOneById(id);
	}
	
	@POST
	@Path("/create")
	public void createDepartement(Departement departement) {
		DaoDepartement daodepartement = new DaoDepartement(Departement.class);
		daodepartement.create(departement);
	}
	
	@POST
	@Path("/delete")
	public void deleteDepartement(Departement departement) {
		DaoDepartement daodepartement = new DaoDepartement(Departement.class);
		daodepartement.delete(departement);
	}
	
	@POST
	@Path("/delete/{id}")
	public void deleteDepartementById(@PathParam("id") long id) {
		DaoDepartement daodepartement = new DaoDepartement(Departement.class);
		daodepartement.deleteById(id);
	}
	
	@POST
	@Path("/update")
	public void updateDepartement(Departement departement) {
		DaoDepartement daodepartement = new DaoDepartement(Departement.class);
		daodepartement.update(departement);
	}
}
