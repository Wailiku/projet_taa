package rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dao.DaoPersonne;
import entity.Personne;

@Path("/personne")
public class RestPersonne {
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Personne> getPersonnes(){
		DaoPersonne daopersonne = new DaoPersonne(Personne.class);
		return daopersonne.findAll();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Personne getPersonneById(@PathParam("id") long id) {
		DaoPersonne daopersonne = new DaoPersonne(Personne.class);
		return daopersonne.findOneById(id);
	}
	
	@POST
	@Path("/create")
	public void createPersonne(Personne personne) {
		DaoPersonne daopersonne = new DaoPersonne(Personne.class);
		daopersonne.create(personne);
	}
	
	@POST
	@Path("/delete")
	public void deletePersonne(Personne personne) {
		DaoPersonne daopersonne = new DaoPersonne(Personne.class);
		daopersonne.delete(personne);
	}
	
	@POST
	@Path("/delete/{id}")
	public void deletePersonneById(@PathParam("id") long id) {
		DaoPersonne daopersonne = new DaoPersonne(Personne.class);
		daopersonne.deleteById(id);
	}
	
	@POST
	@Path("/update")
	public void updatePersonne(Personne personne) {
		DaoPersonne daopersonne = new DaoPersonne(Personne.class);
		daopersonne.update(personne);
	}
}
