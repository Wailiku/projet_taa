package rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dao.DaoVille;
import entity.Ville;

@Path("/ville")
public class RestVille {
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Ville> getVilles(){
		DaoVille daoville = new DaoVille(Ville.class);
		return daoville.findAll();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Ville getVilleById(@PathParam("id") long id) {
		DaoVille daoville = new DaoVille(Ville.class);
		return daoville.findOneById(id);
	}
	
	@POST
	@Path("/create")
	public void createVille(Ville ville) {
		DaoVille daoville = new DaoVille(Ville.class);
		daoville.create(ville);
	}
	
	@POST
	@Path("/delete")
	public void deleteVille(Ville ville) {
		DaoVille daoville = new DaoVille(Ville.class);
		daoville.delete(ville);
	}
	
	@POST
	@Path("/delete/{id}")
	public void deleteVilleById(@PathParam("id") long id) {
		DaoVille daoville = new DaoVille(Ville.class);
		daoville.deleteById(id);
	}
	
	@POST
	@Path("/update")
	public void updateVille(Ville ville) {
		DaoVille daoville = new DaoVille(Ville.class);
		daoville.update(ville);
	}
}
