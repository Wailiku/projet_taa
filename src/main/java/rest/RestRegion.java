package rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dao.*;
import entity.*;

@Path("/region")
public class RestRegion {
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Region> getRegions(){
		DaoRegion daoregion = new DaoRegion(Region.class);
		return daoregion.findAll();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Region getRegionById(@PathParam("id") long id) {
		DaoRegion daoregion = new DaoRegion(Region.class);
		return daoregion.findOneById(id);
	}
	
	@POST
	@Path("/create")
	public void createRegion(Region region) {
		DaoRegion daoregion = new DaoRegion(Region.class);
		daoregion.create(region);
	}
	
	@POST
	@Path("/delete")
	public void deleteRegion(Region region) {
		DaoRegion daoregion = new DaoRegion(Region.class);
		daoregion.delete(region);
	}
	
	@POST
	@Path("/delete/{id}")
	public void deleteRegionById(@PathParam("id") long id) {
		DaoRegion daoregion = new DaoRegion(Region.class);
		daoregion.deleteById(id);
	}
	
	@POST
	@Path("/update")
	public void updateRegion(Region region) {
		DaoRegion daoregion = new DaoRegion(Region.class);
		daoregion.update(region);
	}
	
}
