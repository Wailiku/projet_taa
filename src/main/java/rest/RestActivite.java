package rest;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dao.DaoActivite;
import entity.Activite;
import io.swagger.annotations.Api;
@Api
@Path("/activite")
public class RestActivite {
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Activite> getActivites(){
		DaoActivite daoactivite = new DaoActivite(Activite.class);
		return daoactivite.findAll();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Activite getActiviteById(@PathParam("id") long id) {
		DaoActivite daoactivite = new DaoActivite(Activite.class);
		return daoactivite.findOneById(id);
	}
	
	@POST
	@Path("/create")
	public void createActivite(Activite activite) {
		DaoActivite daoactivite = new DaoActivite(Activite.class);
		daoactivite.create(activite);
	}
	
	@POST
	@Path("/delete")
	public void deleteActivite(Activite activite) {
		DaoActivite daoactivite = new DaoActivite(Activite.class);
		daoactivite.delete(activite);
	}
	
	@POST
	@Path("/delete/{id}")
	public void deleteActiviteById(@PathParam("id") long id) {
		DaoActivite daoactivite = new DaoActivite(Activite.class);
		daoactivite.deleteById(id);
	}
	
	@POST
	@Path("/update")
	public void updateActivite(Activite activite) {
		DaoActivite daoactivite = new DaoActivite(Activite.class);
		daoactivite.update(activite);
	}
}

