# Projet TAA

Nouvelle version du projet TAA du Master 2 Informatique de l'ISTIC.
Fait par Le Mikael et Petit Emilien

Le but du projet est de développer une application permettant à un utilisateur de recevoir un mail tous les mercredi lui indiquant une liste de lieux (ville, département, région) où les conditions météorologiques sont idéales le week-end pour pratiquer des sports nautiques.

Le dépot ici présent contient le back end de l'application et est codé en Java avec Spring Boot. La relation avec le front end sera fait avec une architecture REST, elle est disponible [ici](https://gitlab.com/emilien1111/weekendapp.git). Malheureusement, cela n'a pas été fait car nous n'avons pas réussi à faire le lien en utilisant REST.

# Utilisation de l'application

L'application est un site web où l'utilisateur doit créer un compte avant de poursuivre. Après la création du compte, il devra se connecter pour avoir accés aux fonctionnalités de l'application.

L'utilisateur va ensuite pouvoir choisir les activités qu'il souhaite faire le week-end. La personne devra aussi déterminer où il est possible pour lui de passer son week-end, que ce soit des régions, des départements ou des villes.

Quand toutes ces informations sont connues par l'application, celle-ci devrait envoyer un mail automatiquement tous les mardi soir indiquant les lieux et activités idéaux pour le week-end. Cependant, comme le front end et le back-end ne communiquent pas il nous est impossible de finaliser cette partie. L'utilisateur peut modifier ses informations quand il le souhaite et supprimer son profil.

## Lancement de l'application

La partie back-end se lance en le main de JpaTest.java qui va initialiser la base de données avec des éléments de base. La base de données est celle indiqué dans le fichier persistence.xml et dont le nom est "mysql".

## Ce qu'il reste à faire

Malheureusement, le développement de l'application fut complexe et de ce fait, n'est pas à la hauteur de ce qu'on espère. Il faudrait finir l'implémentation du service REST, l'appel vers l'API de météo, la schedulation des mails et la lien avec le front-end.
